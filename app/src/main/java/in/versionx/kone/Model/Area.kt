package `in`.versionx.kone.Model

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.Ignore
import androidx.room.PrimaryKey

@Entity
class Area {


    @PrimaryKey(autoGenerate = true)
    var id = 0;

    @ColumnInfo(name = "areaId")
    var areaId = ""

    @ColumnInfo(name = "shortNm")
    var nm = ""

    @ColumnInfo(name = "exit")
    var exit = false

    @ColumnInfo(name = "buidId")
    var buidId = ""

    @ColumnInfo(name = "order")
    var order = ""

    @Ignore
    var isSelected = false;
    @Ignore
    var state = 0;

    @Ignore
    var iscurrent = false;

    override fun toString(): String {
        return this.nm
    }

}