package `in`.versionx.kone

import `in`.versionx.kone.Database.StoreDataAsync
import `in`.versionx.kone.Model.Area
import `in`.versionx.kone.Model.Building
import `in`.versionx.kone.Model.Lift
import `in`.versionx.kone.Utils.SchedularService
import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.os.Bundle
import android.os.Handler
import android.util.Log
import android.widget.Switch
import androidx.appcompat.app.AppCompatActivity
import com.android.volley.Request
import com.android.volley.Response
import com.android.volley.toolbox.StringRequest
import com.android.volley.toolbox.Volley
import org.json.JSONObject

class SplashScreenActivity : AppCompatActivity() {

    var sp: SharedPreferences? = null;

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash_screen)

        sp = getSharedPreferences("LOGIN_SP", Context.MODE_PRIVATE)

        startService(Intent(this, SchedularService::class.java)) // call the scheduler to run every 8 sec
        // to get the lift calling status if any in database

        if (sp!!.getBoolean("IS_LOGGED_IN", false)) {

            Handler().postDelayed({ onSuccessLogin() }, 3500)

        } else {
            fetchData(this)

        }


    }

    fun onSuccessLogin() {

        val `in` = Intent(this, HomeActivity::class.java)
        `in`.flags = Intent.FLAG_ACTIVITY_CLEAR_TASK
        `in`.flags = Intent.FLAG_ACTIVITY_CLEAR_TOP
        finish()
        startActivity(`in`)

    }


    fun fetchData(context: Context) {

        val queue = Volley.newRequestQueue(context)


        for (buildingID in App.buildingList) {

            val stringRequest = object : StringRequest(Request.Method.GET, "https://api.kone.com/api/building/" + buildingID,
                    Response.Listener<String> { response ->


                        var obj = JSONObject(response);

                        var buildObj = obj.getJSONObject("collection")
                                .getJSONArray("items").getJSONObject(0).getJSONArray("data");
                        val building: Building = Building();
                        for (i in 0 until buildObj.length()) {
                            val item = buildObj.getJSONObject(i)

                            Log.d("A", "Response is: " +
                                    item)

                            if (item.get("name").equals("id")) {
                                val buildingVal = item.getString("value").split(":");
                                building.id = buildingVal[1]
                            } else if (item.get("name").equals("name")) {
                                building.nm = item.getString("value");
                            } else if (item.get("name").equals("siteName")) {
                                building.siteNm = item.getString("value");
                            }


                        }
                        StoreDataAsync(context, Building::class.simpleName!!, "insert", building, null, null, null).execute();


                    },
                    Response.ErrorListener { }) {
                override fun getHeaders(): MutableMap<String, String> {
                    val headers = HashMap<String, String>()
                    headers["accept"] = "application/vnd.collection+json"
                    headers["x-ibm-client-id"] = "319a265b-7568-4bdd-885d-c42e6c2044dc"
                    headers["x-ibm-client-secret"] = "H0yI6rO5qM5tQ6lV6jY3qF1kG2mT7dX2nC8lU3cR6dF3tP1yJ8"
                    return headers
                }
            }


            queue.add(stringRequest)


            val liftRequest = object : StringRequest(Request.Method.GET, "https://api.kone.com/api/building/" + buildingID + "/lift/",
                    Response.Listener<String> { response ->


                        var obj = JSONObject(response);

                        var liftObj = obj.getJSONObject("collection")
                                .getJSONArray("items");


                        val lifts = ArrayList<Lift>();
                        for (i in 0 until liftObj.length()) {

                            val obj = liftObj.get(i) as JSONObject;

                            val dataArray = obj.getJSONArray("data");
                            var lift = Lift();
                            for (i in 0 until dataArray.length()) {
                                val item = dataArray.getJSONObject(i)
                                Log.d("A", "Response is: " +
                                        item)

                                if (item.get("name").equals("id")) {
                                    val areaVal = item.getString("value").split(":");
                                    lift!!.liftId = areaVal[3]
                                    lift!!.buidId = areaVal[1]
                                    lift!!.groupId = areaVal[2]
                                } else if (item.get("name").equals("name")) {
                                    lift!!.nm = item.getString("value");
                                } else if (item.get("name").equals("type")) {
                                    lift!!.typ = item.getString("value");
                                }


                            }

                            lifts.add(lift)
                            StoreDataAsync(context, Lift::class.simpleName!!, "insert", null, null, lift, null).execute();

                        }
                    },
                    Response.ErrorListener { }) {
                override fun getHeaders(): MutableMap<String, String> {
                    val headers = HashMap<String, String>()
                    headers["accept"] = "application/vnd.collection+json"
                    headers["x-ibm-client-id"] = "319a265b-7568-4bdd-885d-c42e6c2044dc"
                    headers["x-ibm-client-secret"] = "H0yI6rO5qM5tQ6lV6jY3qF1kG2mT7dX2nC8lU3cR6dF3tP1yJ8"
                    return headers
                }
            }


            queue.add(liftRequest)


            val areaRequest = object : StringRequest(Request.Method.GET, "https://api.kone.com/api/building/" + buildingID + "/area/",
                    Response.Listener<String> { response ->


                        var obj = JSONObject(response);

                        var buildObj = obj.getJSONObject("collection")
                                .getJSONArray("items");

                        var areas = ArrayList<Area>();

                        for (i in 0 until buildObj.length()) {

                            val bobj = buildObj.get(i) as JSONObject;
                            val dataArray = bobj.getJSONArray("data");
                            val area = Area();
                            for (i in 0 until dataArray.length()) {
                                val item = dataArray.getJSONObject(i)

                                Log.d("A", "Response is: " +
                                        item)

                                if (item.get("name").equals("id")) {
                                    val areaVal = item.getString("value").split(":");
                                    area.areaId = areaVal[2]
                                    area.buidId = areaVal[1]
                                } else if (item.get("name").equals("shortName")) {
                                    area.nm = item.getString("value");
                                } else if (item.get("name").equals("exit")) {
                                    area.exit = item.getBoolean("value");
                                }

                            }

                            areas.add(area)
                            StoreDataAsync(context, Area::class.simpleName!!, "insert", null, area, null, null).execute();


                        }

                       /* if (buildingID.equals("4tYHwjrhUR")) {
                            var area=Area();
                            area.nm = "3"
                            area.areaId = "5000"
                            area.buidId = "4tYHwjrhUR"

                            StoreDataAsync(context, Area::class.simpleName!!, "insert", null, area, null, null).execute();
                        }*/
                        /* taskFinishListner?.OnAreaTaskFinishListner(areas)*/
                        sp!!.edit().putBoolean("IS_LOGGED_IN", true).apply()

                        onSuccessLogin()

                    },
                    Response.ErrorListener { }) {
                override fun getHeaders(): MutableMap<String, String> {
                    val headers = HashMap<String, String>()
                    headers["accept"] = "application/vnd.collection+json"
                    headers["x-ibm-client-id"] = "319a265b-7568-4bdd-885d-c42e6c2044dc"
                    headers["x-ibm-client-secret"] = "H0yI6rO5qM5tQ6lV6jY3qF1kG2mT7dX2nC8lU3cR6dF3tP1yJ8"
                    return headers
                }
            }


            queue.add(areaRequest)
        }
    }
}