package `in`.versionx.kone.Database

import `in`.versionx.kone.Model.Area
import `in`.versionx.kone.Model.Building
import `in`.versionx.kone.Model.Lift
import `in`.versionx.kone.Model.LiftCalls
import androidx.room.Database
import androidx.room.DatabaseConfiguration
import androidx.room.InvalidationTracker
import androidx.room.RoomDatabase
import androidx.sqlite.db.SupportSQLiteOpenHelper

@Database(entities = [Building::class, Area::class,Lift::class, LiftCalls::class], version = 1)
abstract class AppDatabase : RoomDatabase() {


    abstract fun AreaDao(): AreaDao
    abstract fun BuildingDao(): BuildingDao
    abstract fun LiftDao(): LiftDao
    abstract fun LiftCalls() :LiftCallsDao


}