package `in`.versionx.kone.Database

import `in`.versionx.kone.Model.LiftCalls
import androidx.room.*

@Dao
interface LiftCallsDao {

    @Insert
    fun insertAll(vararg liftCalls: LiftCalls?)

    @Query("DELETE FROM LiftCalls")
    fun deleteAll()
    @Query("DELETE FROM LiftCalls WHERE `id`=:id")
    fun delete(id:String)

    @Query("DELETE FROM LiftCalls WHERE `state`=:state1 OR `state` =:state2")
    fun deleteByState(state1:String,state2:String)

//    @Query("DELETE FROM LiftCalls WHERE `state`=:state1 )
//    fun getLiftCallById(id:String)


    @Query("SELECT * FROM LiftCalls")
    fun getAll():List<LiftCalls>



    @Update(onConflict = OnConflictStrategy.REPLACE)
    fun update(vararg liftCalls: LiftCalls)

}