package `in`.versionx.kone.Database

import `in`.versionx.kone.App
import `in`.versionx.kone.Interface.TaskFinishListner
import `in`.versionx.kone.Model.Area
import `in`.versionx.kone.Model.Building
import `in`.versionx.kone.Model.Lift
import `in`.versionx.kone.Model.LiftCalls
import android.content.Context
import android.content.SharedPreferences
import android.os.AsyncTask
import android.util.Log
import com.android.volley.Request
import com.android.volley.Response
import com.android.volley.toolbox.StringRequest
import com.android.volley.toolbox.Volley
import org.json.JSONObject

class StoreDataAsync(val context: Context, val table: String, val action: String,
                     val building: Building?, val area: Area?, val lift: Lift?, val liftCalls: LiftCalls?) : AsyncTask<Void, Void, String>() {

    var taskFinishListner: TaskFinishListner? = null
    var calls: List<LiftCalls>? = null
    var buildings: List<Building>? = null
    var areas: List<Area>? = null
    lateinit var loginSp: SharedPreferences;


    fun setListner(taskFinishListner: TaskFinishListner) {
        this.taskFinishListner = taskFinishListner
        loginSp = context.getSharedPreferences("KONE_SP", Context.MODE_PRIVATE);
    }


    override fun doInBackground(vararg p0: Void?): String {

        when (action) {

            "insert" -> {
                if (table.equals(Building::class.simpleName))
                    App.getDbInstance()!!.BuildingDao().insertAll(building!!)
                else if (table.equals(Area::class.simpleName))
                    App.getDbInstance()!!.AreaDao().insertAll(area!!)
                else if (table.equals(Lift::class.simpleName))
                    App.getDbInstance()!!.LiftDao().insertAll(lift!!)
                else if (table.equals(LiftCalls::class.simpleName)) {
                    App.getDbInstance()!!.LiftCalls().deleteAll()
                    App.getDbInstance()!!.LiftCalls().insertAll(liftCalls!!)

                    //  App.getDbInstance()!!.LiftCalls().deleteByState("served", "cancelled")
                }
            }
            "deleteAll" -> {
                //   App.getDbInstance()!!.BuildingDao().deleteAll()
                //   App.getDbInstance()!!.AreaDao().deleteAll()
                //  App.getDbInstance()!!.LiftDao().deleteAll()
                App.getDbInstance()!!.LiftCalls().deleteAll()

            }
            "get" -> {
                if (table.equals(LiftCalls::class.simpleName))
                    calls = App.getDbInstance()!!.LiftCalls().getAll()
                if (table.equals(Area::class.simpleName))
                    areas = App.getDbInstance()!!.AreaDao().getAreaById(loginSp.getString("BID", "")!!);
                if (table.equals(Building::class.simpleName))
                    buildings = App.getDbInstance()!!.BuildingDao().getAll()
            }

            "update" -> {


                if (table.equals(LiftCalls::class.simpleName))
                    App.getDbInstance()!!.LiftCalls().update(liftCalls!!)
            }
            "delete" -> {
                if (table.equals(LiftCalls::class.simpleName)) {
                    App.getDbInstance()!!.LiftCalls().deleteAll()
                    // App.getDbInstance()!!.LiftCalls().delete(liftCalls!!.id)
                }

            }


        }

        return "inserted";
    }

    override fun onPostExecute(result: String?) {
        super.onPostExecute(result)

        if (action.equals("deleteAll")) {
            //  fetchData()

        }
        if (action.equals("get") && table.equals(LiftCalls::class.simpleName)) {
            taskFinishListner!!.OnLiftCallFinishListner(calls!!)
        }

        if (action.equals("get") && table.equals(Area::class.simpleName)) {
            taskFinishListner?.OnAreaTaskFinishListner(areas!!)
        }


        if (action.equals("get") && table.equals(Building::class.simpleName)) {
            taskFinishListner?.OnBuildingTaskFinishListner(buildings!!)
        }


        if ((action.equals("update") || action.equals("delete")) && table.equals(LiftCalls::class.simpleName)) {
            taskFinishListner!!.onUpdateLiftCalls()
        }
    }


}