package `in`.versionx.kone.Model

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity
class Lift {

    @PrimaryKey(autoGenerate = true)
    var id = 0;

    @ColumnInfo(name = "liftId")
    var liftId = ""

    @ColumnInfo(name = "nm")
    var nm = ""

    @ColumnInfo(name = "typ")
    var typ = ""

    @ColumnInfo(name = "buidId")
    var buidId = ""

    @ColumnInfo(name = "groupId")
    var groupId = ""

    override fun toString(): String {
        return this.nm
    }
}