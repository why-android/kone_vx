package `in`.versionx.kone.Model

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity
class Building {

    @PrimaryKey
    var id ="";

    @ColumnInfo(name = "nm")
    var nm="";

    @ColumnInfo(name ="siteNm")
    var siteNm="";

}