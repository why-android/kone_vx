package `in`.versionx.kone.Adapter

import `in`.versionx.kone.Model.Lift
import `in`.versionx.kone.Model.LiftCalls
import `in`.versionx.kone.R
import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.recyclerview.widget.RecyclerView
import com.android.volley.NetworkResponse
import com.android.volley.Response
import com.android.volley.toolbox.JsonObjectRequest
import com.android.volley.toolbox.Volley
import kotlinx.android.synthetic.main.lift_item.view.*
import org.json.JSONArray
import org.json.JSONObject


class LiftAdapter(val listItem: ArrayList<LiftCalls>, val context: Context) : RecyclerView.Adapter<LiftAdapter.ViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val context = parent.context
        val inflater = LayoutInflater.from(context)

        // Inflate the custom layout

        // Inflate the custom layout
        val view: View = inflater.inflate(R.layout.lift_item, parent, false)

        // Return a new holder instance
        val viewHolder = ViewHolder(view)
        // Return a new holder instance

        return viewHolder
    }

    override fun getItemViewType(position: Int): Int {
        return position
    }

    override fun getItemCount(): Int {

        return listItem.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {

        holder.lift_nm.setText(listItem.get(position).state)
        holder.call_id.setText(listItem.get(position).id)
        holder.lift_nm.setTag(listItem.get(position))
        holder.call_floor.setText(listItem.get(position).source+" - "+listItem.get(position).destination)
        holder.pass_status.setText(listItem.get(position).passGuidance)

    }

    class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {

        val lift_nm = view.lift_nm
        val call_id =view.call_id
        val call_floor= view.call_floor
        val pass_status =view.pass_status
    }


}