package `in`.versionx.kone

import `in`.versionx.kone.Adapter.LiftAdapter_new
import `in`.versionx.kone.Adapter.SpinAdapter
import `in`.versionx.kone.Database.StoreDataAsync
import `in`.versionx.kone.Interface.TaskFinishListner
import `in`.versionx.kone.Model.Area
import `in`.versionx.kone.Model.Building
import `in`.versionx.kone.Model.Lift
import `in`.versionx.kone.Model.LiftCalls
import `in`.versionx.kone.Utils.CryptLib
import `in`.versionx.kone.Utils.SchedularService
import android.content.*
import android.media.MediaPlayer
import android.os.Bundle
import android.util.Log
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.*
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.android.volley.*
import com.android.volley.toolbox.JsonObjectRequest
import com.android.volley.toolbox.StringRequest
import com.android.volley.toolbox.Volley
import com.google.firebase.analytics.FirebaseAnalytics
import com.google.firebase.analytics.ktx.analytics
import com.google.firebase.analytics.ktx.logEvent
import com.google.firebase.ktx.Firebase
import com.google.zxing.integration.android.IntentIntegrator
import org.json.JSONArray
import org.json.JSONObject
import kotlin.collections.ArrayList
import kotlin.collections.HashMap


class HomeActivity : AppCompatActivity(), TaskFinishListner, LiftAdapter_new.floorSelect {

    var schedularintent: Intent? = null
    var rv_lift: RecyclerView? = null;
    var current_floor: TextView? = null
    var tv_state: TextView? = null
    var select_building_id: Spinner? = null
    var tv_lift: TextView? = null
    var lifts: ArrayList<Lift>? = null;
    var source: Area? = null;
    var destination: Area? = null;
    var btn_call: ImageButton? = null
    var ll_qr_code: LinearLayout? = null
    var tv_building_nm: TextView? = null
    var ll_current_floor: LinearLayout? = null;
    var liftCalls = ArrayList<LiftCalls>();
    var isLiftCalled = false;

    var areaList = ArrayList<Area>();

    var qrbuildingId: String = "";
    var qrsourceArea: String = "";
    var currentFloor = Area();
    var mp: MediaPlayer? = null;
    var queue: RequestQueue? = null;
    private lateinit var firebaseAnalytics: FirebaseAnalytics
    lateinit var loginSp: SharedPreferences;
    var counter: Long=0;


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_home)
        val storeDataAsync1 = StoreDataAsync(this, Building::class.simpleName!!, "deleteAll", null, null, null, null);
        storeDataAsync1.setListner(this)
        storeDataAsync1.execute();

        val storeDataAsync3 = StoreDataAsync(this, Building::class.simpleName!!, "get", null, null, null, null);
        storeDataAsync3.setListner(this)
        storeDataAsync3.execute();



        initUI()
        //  startService(Intent(this, SchedularService::class.java))
        /*schedularintent = Intent(this, SchedularService::class.java)
         if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) startForegroundService(schedularintent) else startService(schedularintent)
   */
    }

    fun initUI() {
        queue = Volley.newRequestQueue(this)
        mp = MediaPlayer.create(this, R.raw.elevator_sound)
        lifts = ArrayList();
        rv_lift = findViewById(R.id.rv_lift);
        current_floor = findViewById(R.id.current_floor);
        tv_state = findViewById(R.id.tv_state);
        select_building_id = findViewById(R.id.select_building_id);
        tv_lift = findViewById(R.id.tv_lift);
        btn_call = findViewById(R.id.btn_call);
        ll_qr_code = findViewById(R.id.ll_qr_code);
        ll_current_floor = findViewById(R.id.ll_current_floor);
        rv_lift!!.layoutManager = LinearLayoutManager(this)

        btn_call!!.setOnClickListener(View.OnClickListener {


            scanQrCode()
            // callLiftApi(source!!, destination!!);
        })
        //  rv_lift!!.adapter = LiftAdapter(liftCalls!!, this)
        rv_lift!!.setLayoutManager(GridLayoutManager(this, 3))
        var floorAdapter = LiftAdapter_new(areaList, this);
        rv_lift!!.adapter = floorAdapter;
        floorAdapter.setFloorListner(this)

        supportActionBar!!.setTitle("VersionX Lift - SJR Luxuria")
        firebaseAnalytics = Firebase.analytics

        loginSp = getSharedPreferences("KONE_SP", Context.MODE_PRIVATE)


        select_building_id!!.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onNothingSelected(parent: AdapterView<*>?) {

            }

            override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {

                loginSp.edit().putString("BID", App.buildingList[position]).apply()

            }

        }
    }


    override fun OnAreaTaskFinishListner(area: List<Area>) {

        if(area!=null && area.size>0) {
            areaList.clear()
            currentFloor.nm = "";
            areaList.addAll(sortArea(area));

            System.out.println("current floor " + currentFloor.nm)

            if (currentFloor != null && currentFloor.nm != null && !currentFloor.nm.isEmpty()) {
                for (dest in areaList) {
                    if (dest.nm.equals(currentFloor.nm) || (currentFloor.nm.contains("R") && ((dest.nm).equals(currentFloor.nm.replace("R", ""))))
                            || (!currentFloor.nm.contains("R")) && ((dest.nm).equals((currentFloor.nm) + "R"))) {
                        dest.iscurrent = true
                    } else
                        dest.iscurrent = false


                }
            } else {

                for (dest in areaList) {
                    if (dest.areaId.equals(qrsourceArea))
                        dest.iscurrent = true
                    else
                        dest.iscurrent = false


                }

            }
            rv_lift!!.adapter!!.notifyDataSetChanged()
        }
        else
        {
            Toast.makeText(this,"No Area Access",Toast.LENGTH_LONG).show()
        }
    }


    private fun sortArea(areas: List<Area>): ArrayList<Area> {



        var list: ArrayList<Area> = ArrayList();

        list.addAll(areas)

        if (areas.get(0).buidId.equals("9990000508")) {
            for (area in areas) {
                if (area.areaId.equals(qrsourceArea)) {
                    currentFloor = area;
                }
                when (area.nm) {
                    "K1" -> list.set(0, area);
                    "K1R" -> list.set(1, area);
                    "K2" -> list.set(2, area);
                    "K2R" -> list.set(3, area);
                    "1" -> list.set(4, area);
                    "1R" -> list.set(5, area);
                    "2" -> list.set(6, area);
                    "2R" -> list.set(7, area);
                    "3" -> list.set(8, area);
                    "3R" -> list.set(9, area);
                    "4" -> list.set(10, area);
                    "4R" -> list.set(11, area);
                    "5" -> list.set(12, area);
                    "5R" -> list.set(13, area);
                    "6" -> list.set(14, area);
                    "6R" -> list.set(15, area);
                    "7" -> list.set(16, area);
                    "7R" -> list.set(17, area);
                    "8" -> list.set(18, area);
                    "8R" -> list.set(19, area);
                    "9" -> list.set(20, area);
                    "9R" -> list.set(21, area);
                    "10" -> list.set(22, area);
                    "10R" -> list.set(23, area);
                    "11" -> list.set(24, area);
                    "11R" -> list.set(25, area);
                    "12" -> list.set(26, area);
                    "12R" -> list.set(27, area);
                    "13" -> list.set(28, area);
                    "13R" -> list.set(29, area);
                    "14" -> list.set(30, area);
                    "14R" -> list.set(31, area);
                    "15" -> list.set(32, area);
                    "15R" -> list.set(33, area);
                    "16" -> list.set(34, area);
                    "16R" -> list.set(35, area);
                    "17" -> list.set(36, area);
                    "17R" -> list.set(37, area);
                    "18" -> list.set(38, area);
                    "18R" -> list.set(39, area);

                }

            }
        }

        System.out.println("list is alka " + list.size)
        return list;

    }


    override fun OnBuildingTaskFinishListner(buildings: List<Building>) {
        val array = arrayListOf<Building>()
        array.addAll(buildings)

        val buildingApdater = SpinAdapter(this, R.layout.spinner_, array);
        select_building_id!!.adapter = buildingApdater
    }

    override fun OnLiftCallFinishListner(calls: List<LiftCalls>) {

        if (calls.size > 0) {
            try {
                liftCalls.clear()
                liftCalls.addAll(calls)



                if (!liftCalls.get(0).state.equals("cancelled", true)) {
                    for (floor in areaList) {
                        if (floor.nm.equals(liftCalls.get(0).destination)) {
                            if (liftCalls.get(0).state.equals("served")) {
                                floor.state = 2;

                            } else {
                                floor.state = 1;
                            }


                        } else {
                            floor.state = 0;
                        }

                    }

                  //  rv_lift!!.adapter!!.notifyDataSetChanged() ///// Alka Aswal


                    /*    val stringRequest = object : StringRequest(Request.Method.GET, liftCalls.get(0).carLink,
                                Response.Listener<String> { response ->

                                    System.out.println("data here " + response)
                                    var obj = JSONObject(response);
                                    var buildObj = obj.getJSONObject("collection")
                                            .getJSONArray("items").getJSONObject(0).getJSONArray("links");


                                    for (i in 0 until buildObj.length()) {
                                        val item = buildObj.getJSONObject(i)

                                        Log.d("A", "Response is alka :" +
                                                item)


                                        if (item.get("rel").equals("currentFloor")) {
                                            val floorVal = item.getString("href")
                                            //  val lastIndex = floorVal.lastIndexOf("/");
                                            //  val currentFloor = floorVal.substring(lastIndex + 1, floorVal.length);
                                            System.out.println("alka floor is " + floorVal)
                                            val floorRequest = object : StringRequest(Request.Method.GET, floorVal,
                                                    Response.Listener<String> { response ->
                                                        try {
                                                            System.out.println("data here " + response)
                                                            var obj = JSONObject(response);
                                                            var buildObj = obj.getJSONObject("collection")
                                                                    .getJSONArray("items").getJSONObject(0).getJSONArray("data");


                                                            for (i in 0 until buildObj.length()) {
                                                                val item = buildObj.getJSONObject(i)

                                                                Log.d("A", "Response is: " +
                                                                        item)


                                                                if (item.get("name").equals("floorMark")) {
                                                                    val floorVal = item.getString("value")
                                                                         System.out.println("current floor" + floorVal)
                                                                    current_floor!!.setText(floorVal)
                                                                    tv_state!!.visibility = View.VISIBLE
                                                                    tv_state!!.setText(getLiftState(liftCalls.get(0).passGuidance, floorVal))
                                                                }


                                                            }
                                                        } catch (e: Exception) {
                                                            Toast.makeText(this, e.message, Toast.LENGTH_LONG).show()
                                                        }

                                                    },
                                                    Response.ErrorListener { }) {
                                                override fun getHeaders(): MutableMap<String, String> {
                                                    val headers = HashMap<String, String>()
                                                    headers["accept"] = "application/vnd.collection+json"
                                                    headers["x-ibm-client-id"] = "319a265b-7568-4bdd-885d-c42e6c2044dc"
                                                    headers["x-ibm-client-secret"] = "H0yI6rO5qM5tQ6lV6jY3qF1kG2mT7dX2nC8lU3cR6dF3tP1yJ8"
                                                    return headers
                                                }
                                            }

                                            queue!!.add(floorRequest)


                                        }


                                    }

                                },
                                Response.ErrorListener { }) {
                            override fun getHeaders(): MutableMap<String, String> {
                                val headers = HashMap<String, String>()
                                headers["accept"] = "application/vnd.collection+json"
                                headers["x-ibm-client-id"] = "319a265b-7568-4bdd-885d-c42e6c2044dc"
                                headers["x-ibm-client-secret"] = "H0yI6rO5qM5tQ6lV6jY3qF1kG2mT7dX2nC8lU3cR6dF3tP1yJ8"
                                return headers
                            }
                        }
                        queue!!.add(stringRequest)
    */

                    val floorRequest = object : StringRequest(Request.Method.GET, liftCalls.get(0).nameLink,
                            Response.Listener<String> { response ->
                                try {
                                    System.out.println("data here " + response)
                                    var obj = JSONObject(response);
                                    var buildObj = obj.getJSONObject("collection")
                                            .getJSONArray("items").getJSONObject(0).getJSONArray("data");


                                    for (i in 0 until buildObj.length()) {
                                        val item = buildObj.getJSONObject(i)

                                        Log.d("A", "Response is: " +
                                                item)


                                        if (item.get("name").equals("name")) {
                                            val liftVal = item.getString("value")
                                            //  val lastIndex = floorVal.lastIndexOf("/");
                                            //  val currentFloor = floorVal.substring(lastIndex + 1, floorVal.length);
                                            System.out.println("current lift" + liftVal)
                                            current_floor!!.setText("")
                                            tv_lift!!.visibility = View.VISIBLE
                                            tv_lift!!.setText("LIFT" + "-" + liftVal)
                                        }


                                        val storeDataAsync = StoreDataAsync(this, LiftCalls::class.simpleName!!, "delete", null, null, null, null);
                                        storeDataAsync.setListner(this)
                                        storeDataAsync.execute();


                                    }
                                } catch (e: Exception) {
                                    //  Toast.makeText(this, e.message, Toast.LENGTH_LONG).show()
                                }

                            },
                            Response.ErrorListener { }) {
                        override fun getHeaders(): MutableMap<String, String> {
                            val headers = HashMap<String, String>()
                            headers["accept"] = "application/vnd.collection+json"
                            headers["x-ibm-client-id"] = "319a265b-7568-4bdd-885d-c42e6c2044dc"
                            headers["x-ibm-client-secret"] = "H0yI6rO5qM5tQ6lV6jY3qF1kG2mT7dX2nC8lU3cR6dF3tP1yJ8"
                            return headers
                        }
                    }
                    queue!!.add(floorRequest)
                } else {

                    Toast.makeText(this, getCancelReason(liftCalls.get(0).cancel_reason), Toast.LENGTH_LONG).show()
                    updateUI(false, "cancelled", liftCalls.get(0).cancel_reason)
                    val storeDataAsync = StoreDataAsync(this, LiftCalls::class.simpleName!!, "delete", null, null, null, null);
                    storeDataAsync.setListner(this)
                    storeDataAsync.execute();

                }
            } catch (e: Exception) {
                updateUI(false, "cancelled", "");
                val storeDataAsync = StoreDataAsync(this, LiftCalls::class.simpleName!!, "delete", null, null, null, null);
                storeDataAsync.setListner(this)
                storeDataAsync.execute();
            }
        }


    }

    fun getLiftState(state: String, floorVal: String): String {
        System.out.println("service alka " + state)
        //Toast.makeText(this, "Status " + state, Toast.LENGTH_SHORT).show()

        when (state) {
            "wait_for_car" -> return "WAIT"
            "enter_car" -> {
                if (source!!.nm.equals(floorVal)) {
                    updateUI(true, "ARRIVED", "")
                    playSound();
                    return "ARRIVED"
                }

                updateUI(true, "ARRIVING", "")
                return "ARRIVING"
            }
            "stay_in_car" -> {
                if (source!!.nm.equals(floorVal)) {
                    updateUI(true, "CLOSING", "")
                    return "CLOSING"
                }
                updateUI(true, "CLOSED", "")
                return "CLOSED"
            }
            "exit_car" -> {
                //  if (destination!!.nm.equals(floorVal)) {
                updateUI(false, "REACHING", "")
                playSound();
                return "REACHING"
                //   }
                //  updateUI(true, "REACHING")
                //  return "REACHING"
            }
            "call_cancelled" -> {
                updateUI(false, "cancelled", "")
                return "CANCELED"
            }
        }

        return ""

    }

    fun playSound() {

        try {
            if (mp != null && !mp!!.isPlaying) {
                try {
                    mp!!.start()

                    mp!!.setOnCompletionListener {
                        mp!!.stop();
                        if (mp != null) {
                            mp!!.stop()
                            mp!!.release();
                            mp = null;

                        }
                    }
                } catch (e: Exception) {

                    if (mp != null) {
                        mp!!.stop()
                        mp!!.release();
                        mp = null;

                    }

                }
            }
        } catch (e: Exception) {

            if (mp != null) {
                mp!!.stop()
                mp!!.release();
                mp = null;

            }

        }
    }

    fun updateUI(isLiftCalled: Boolean, status: String, cancelReason: String) {
        this.isLiftCalled = isLiftCalled;


        /* if (status.equals("arriving", true) || status.equals("reaching", true)
                 || status.equals("arrived", true))
             ll_current_floor!!.setBackgroundColor(Color.parseColor("#BF28B463"))
         else if (status.equals("reached", true)
                 || status.equals("closing", true) || status.equals("closed", true))
             ll_current_floor!!.setBackgroundColor(Color.parseColor("#BFFF0000"))
         else
             ll_current_floor!!.setBackgroundColor(Color.parseColor("#FFFFFF"))*/

        if (status.equals("cancelled")) {
            if (!cancelReason.isEmpty())
                current_floor!!.setText(getCancelReason(cancelReason))
            else
                current_floor!!.setText("CALL CANCELED! Please Try agian")
        }


        if (isLiftCalled) {
            rv_lift!!.visibility = View.VISIBLE
            ll_qr_code!!.visibility = View.GONE
            ll_current_floor!!.visibility = View.VISIBLE


        } else {
            rv_lift!!.visibility = View.GONE
            ll_qr_code!!.visibility = View.VISIBLE
            ll_current_floor!!.visibility = View.GONE

        }



        invalidateOptionsMenu()
    }

    override fun onUpdateLiftCalls() {

    }

    fun scanQrCode() {


        val integrator = IntentIntegrator(this)
        integrator.setDesiredBarcodeFormats(IntentIntegrator.ONE_D_CODE_TYPES)
        integrator.setDesiredBarcodeFormats(IntentIntegrator.QR_CODE_TYPES)
        integrator.setPrompt("Scan QR code") // Wide scanning rectangle, may work better for 1D barcodes
        integrator.setCameraId(0) // Use a specific camera of the device
        integrator.initiateScan()
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.home_menu, menu)

        val menuItem: MenuItem = menu!!.findItem(R.id.scan_setting)
        if (isLiftCalled) {
            menuItem.setVisible(true)
        } else {
            menuItem.setVisible(false)
        }
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            R.id.scan_setting -> {


                scanQrCode()
                true
            }

            else -> super.onOptionsItemSelected(item)
        }
    }


    fun callLiftApi(buildingId: String, sourceAreaId: String, destination: Area) {

        SchedularService.counter =0;
        //  var sourceArea: Area? = null;
        for (area in areaList) {
            if (area.areaId.equals(sourceAreaId)) {
                source = area
            }
        }


        firebaseAnalytics.logEvent(FirebaseAnalytics.Event.SELECT_ITEM) {
            param(FirebaseAnalytics.Param.ITEM_ID, buildingId + "_LIFT CALL")
            param(FirebaseAnalytics.Param.ITEM_NAME, source!!.nm + " to " + destination.nm)
            param(FirebaseAnalytics.Param.CONTENT_TYPE, "text")
        }


        var data = JSONArray()
        var sourceA = JSONObject();
        var direction = JSONObject();
        sourceA.put("name", "sourceAreaId")
        sourceA.put("value", "area:" + buildingId + ":" + source!!.areaId)

        /* direction.put("name", "direction")
         direction.put("value", "up")*/


        var des = JSONObject();
        des.put("name", "destinationAreaId")
        des.put("value", "area:" + buildingId + ":" + destination.areaId)
        //  data.put(direction)
        data.put(sourceA)
        data.put(des)


        var template = JSONObject()
        template.put("data", data)

        var main = JSONObject()
        main.put("template", template)


        val jsonobj: JsonObjectRequest = object : JsonObjectRequest(Method.POST, "https://api.kone.com/api/building/" + buildingId + "/call/", main,
                Response.Listener { response ->
                    System.out.println("response list " + response)

                    var obj = response;

                    var buildObj = obj.getJSONObject("collection")
                            .getJSONArray("items");



                    for (i in 0 until buildObj.length()) {

                        val bobj = buildObj.get(i) as JSONObject;
                        val dataArray = bobj.getJSONArray("data");
                        var liftCall = LiftCalls();
                        for (i in 0 until dataArray.length()) {
                            val item = dataArray.getJSONObject(i)

                            Log.d("A", "Response is: " +
                                    item)

                            if (item.get("name").equals("id")) {
                                val areaVal = item.getString("value").split(":");
                                liftCall.id = areaVal[2]
                                liftCall.buildId = areaVal[1]
                            } else if (item.get("name").equals("callState")) {
                                liftCall.state = item.getString("value");
                                if (liftCall.state.equals("created")) {
                                    counter = System.currentTimeMillis();
                                } else {
                                    counter = 0;
                                }
                            } else if (item.get("name").equals("passengerGuidance")) {
                                liftCall.passGuidance = item.getString("value");
                            } else if (item.get("name").equals("cancelReason")) {
                                liftCall.cancel_reason = item.getString("value");
                            }


                            if (counter>0 && ((System.currentTimeMillis() - counter) >= 500)) {
                                liftCall.state = "cancelled";
                                liftCall.cancel_reason = "Connection_TimeOut";
                            }

                        }

                        liftCall.source = source!!.nm;
                        liftCall.destination = destination!!.nm;
                        liftCalls.clear()
                        liftCalls.add(liftCall)

                        StoreDataAsync(this, LiftCalls::class.simpleName!!, "insert", null, null, null, liftCall).execute();

                        updateUI(true, liftCall.state, liftCall.cancel_reason);
                    }

                },
                Response.ErrorListener { volleyError ->

                    System.out.println("lift error " + volleyError)
                    counter =0;
                    var message: String? = null
                    if (volleyError is NetworkError) {
                        message = "Cannot connect to Internet...Please check your connection!"
                    } else if (volleyError is ServerError) {
                        message = "The server could not be found. Please try again after some time!!"
                    } else if (volleyError is AuthFailureError) {
                        message = "Cannot connect to Internet...Please check your connection!"
                    } else if (volleyError is ParseError) {
                        message = "Parsing error! Please try again after some time!!"
                    } else if (volleyError is NoConnectionError) {
                        message = "Cannot connect to Internet...Please check your connection!"
                    } else if (volleyError is TimeoutError) {
                        message = "Connection TimeOut! Please check your internet connection."


                    }

                    Toast.makeText(this, message, Toast.LENGTH_LONG).show()
                    updateUI(false, "cancelled", "");
                }

        ) {
            override fun getHeaders(): MutableMap<String, String> {
                val headers = HashMap<String, String>()
                headers["accept"] = "application/vnd.collection+json"
                headers["content-type"] = "application/vnd.collection+json"
                headers["x-ibm-client-id"] = "319a265b-7568-4bdd-885d-c42e6c2044dc"
                headers["x-ibm-client-secret"] = "H0yI6rO5qM5tQ6lV6jY3qF1kG2mT7dX2nC8lU3cR6dF3tP1yJ8"
                return headers
            }

            override fun parseNetworkResponse(response: NetworkResponse?): Response<JSONObject> {
                System.out.println("response code " + response!!.statusCode)


                if (response!!.statusCode == 201) {


                }
                return super.parseNetworkResponse(response)
            }

        }
        jsonobj!!.setRetryPolicy(DefaultRetryPolicy(10000,
                0,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        queue!!.add(jsonobj)

    }


    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {

        if (requestCode == 49374) {

            if (resultCode == RESULT_OK) {

                for (item in areaList) {
                    item.isSelected = false;
                    item.state = 0
                    item.iscurrent = false
                }
                rv_lift!!.adapter!!.notifyDataSetChanged()
                queue?.cancelAll({ true })
                val storeDataAsync = StoreDataAsync(this, LiftCalls::class.simpleName!!, "delete", null, null, null, null);
                storeDataAsync.setListner(this)
                storeDataAsync.execute();



                current_floor!!.setText("SELECT FLOOR")
                tv_state!!.setText("")
                tv_lift!!.setText("")
                tv_state!!.visibility = View.GONE
                tv_lift!!.visibility = View.GONE

                val scanningResult = IntentIntegrator.parseActivityResult(requestCode, resultCode, data)

                System.out.println("scanned data " + scanningResult.getContents())
                var scannedData: String;
                val cryptLib = CryptLib()
                if (!scanningResult.getContents().startsWith("#LFT")) {
                    scannedData = cryptLib.decryptSimple(scanningResult.getContents(), "versionx", "123")
                    if (scannedData.startsWith("#LFT")) {
                        val data = scannedData.split("|")
                        qrbuildingId = data[1]
                        qrsourceArea = data[2]
                        loginSp.edit().putString("BID", qrbuildingId).apply()



                        tv_lift!!.visibility = View.GONE
                        tv_state!!.visibility = View.GONE

                        updateUI(true, "WAIT", "")

                    }


                } else {

                    val data = scanningResult.getContents().split("|")
                    qrbuildingId = data[1]
                    loginSp.edit().putString("BID", qrbuildingId).apply()
                    qrsourceArea = data[2]
                    rv_lift!!.adapter!!.notifyDataSetChanged()

                    updateUI(true, "WAIT", "")

                }


                val storeDataAsync2 = StoreDataAsync(this, Area::class.simpleName!!, "get", null, null, null, null);
                storeDataAsync2.setListner(this)
                storeDataAsync2.execute();


                // Toast.makeText(this, qrsourceArea, Toast.LENGTH_SHORT).show()


            }

        }
        super.onActivityResult(requestCode, resultCode, data)

    }


    private val receiver: BroadcastReceiver = object : BroadcastReceiver() {
        override fun onReceive(context: Context?, intent: Intent) {

            val asyncCall = StoreDataAsync(context!!, LiftCalls::class.simpleName!!, "get", null, null, null, null);
            asyncCall.setListner(this@HomeActivity)
            asyncCall.execute()

        }
    }

    override fun onResume() {
        super.onResume()
        registerReceiver(receiver, IntentFilter("UPDATE_RESULT"));
    }

    override fun onDestroy() {
        super.onDestroy()

        SchedularService.counter =0;

        unregisterReceiver(receiver)
        if (mp != null) {
            mp!!.stop()
            mp!!.release();
            mp = null;

        }
    }

    override fun onFloorSelect() {

        System.out.println("selected " + areaList)

        current_floor!!.setText("CALLING LIFT...")
        for (dest in areaList) {
            if (dest.isSelected) {
                destination = dest
                callLiftApi(qrbuildingId, qrsourceArea, destination!!); // call lift api
                break
            }

        }


    }

    fun getCancelReason(cancelReason: String): String {
        var cancelMsg: String = ""

        var str = cancelReason.split("_");

        for (words in str) {
            cancelMsg = cancelMsg + capitalize(words) + " ";
        }

        return cancelMsg;

    }

    private fun capitalize(line: String): String? {
        return Character.toUpperCase(line[0]).toString() + line.substring(1)
    }


}
