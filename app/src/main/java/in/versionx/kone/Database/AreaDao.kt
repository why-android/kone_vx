package `in`.versionx.kone.Database

import `in`.versionx.kone.Model.Area
import `in`.versionx.kone.Model.Building
import `in`.versionx.kone.Model.LiftCalls
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.Query

@Dao
interface AreaDao {

    @Insert
    fun insertAll(vararg area: Area?)

    @Query("DELETE FROM Area")
    fun deleteAll()

    @Query("SELECT * FROM Area")
    fun getAll():List<Area>

    @Query("SELECT * FROM Area Where `buidId`= :id")
    fun getAreaById(id:String):List<Area>


}