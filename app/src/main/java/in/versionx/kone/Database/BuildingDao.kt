package `in`.versionx.kone.Database

import `in`.versionx.kone.Model.Area
import `in`.versionx.kone.Model.Building
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.Query

@Dao
interface BuildingDao {

    @Insert
    fun insertAll(vararg building: Building?)

    @Query("DELETE FROM Building")
    fun deleteAll()

    @Query("SELECT * FROM Building")
    fun getAll():List<Building>

}