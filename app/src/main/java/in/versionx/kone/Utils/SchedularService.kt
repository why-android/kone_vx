package `in`.versionx.kone.Utils

import `in`.versionx.kone.App
import `in`.versionx.kone.Database.StoreDataAsync
import `in`.versionx.kone.Interface.TaskFinishListner
import `in`.versionx.kone.Model.Area
import `in`.versionx.kone.Model.Building
import `in`.versionx.kone.Model.LiftCalls
import android.annotation.TargetApi
import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.Service
import android.content.Context
import android.content.Intent
import android.graphics.Color
import android.os.Build
import android.os.Handler
import android.os.IBinder
import android.util.Log
import android.widget.Toast
import androidx.core.app.NotificationCompat
import com.android.volley.*
import com.android.volley.toolbox.StringRequest
import com.android.volley.toolbox.Volley
import org.json.JSONObject


class SchedularService : Service(), TaskFinishListner {
    var isServiceStarted: Boolean = false
    private var intentReceiver: Intent? = null
    var handler: Handler? = null
    var test: Runnable? = null
    companion object {
        var counter : Long = 0
    }

    override fun onBind(intent: Intent?): IBinder? {
        return null
    }

    override fun onCreate() {
        super.onCreate()
/*
        val mNotifyManager = getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) createChannel(mNotifyManager)
        val mBuilder = NotificationCompat.Builder(this, "Elevator")
                .setColor(Color.parseColor("#000000")).setContentTitle("KoneVX").setContentText("Elevator")

        startForeground(1, mBuilder.build())*/

    }

    @TargetApi(26)
    private fun createChannel(notificationManager: NotificationManager) {
        val name = "Elevator"
        val description = "Elevator"
        val importance = NotificationManager.IMPORTANCE_DEFAULT
        val mChannel = NotificationChannel(name, name, importance)
        mChannel.description = description
        mChannel.enableLights(true)
        mChannel.lightColor = Color.BLUE
        notificationManager.createNotificationChannel(mChannel)
    }

    override fun onStartCommand(intent: Intent?, flags: Int, startId: Int): Int {
        if (!isServiceStarted) {
            handler = Handler()
            test = Runnable {
                Log.d("foo", "bar")
                fetchData()
                handler!!.postDelayed(test, 800) //100 ms you should do it 4000
            }
            handler!!.postDelayed(test, 800)//100 ms you should do it 4000
        }

        isServiceStarted = true

        return Service.START_STICKY
    }

    override fun onDestroy() {
        super.onDestroy()
        isServiceStarted = false

    }


    fun fetchData() {
        val asyncCall = StoreDataAsync(this, LiftCalls::class.simpleName!!, "get", null, null, null, null);
        asyncCall.setListner(this)
        asyncCall.execute()


    }

    override fun OnAreaTaskFinishListner(area: List<Area>) {

    }

    override fun OnBuildingTaskFinishListner(calls: List<Building>) {
    }

    override fun OnLiftCallFinishListner(calls: List<LiftCalls>) {

        var buildingID = getSharedPreferences("KONE_SP", Context.MODE_PRIVATE).getString("BID", "")


        for (liftCalls in calls) {
            if (!liftCalls.state.equals("served")) {
                val queue = Volley.newRequestQueue(this)

                val stringRequest = object : StringRequest(Request.Method.GET, "https://api.kone.com/api/building/" + buildingID + "/call/" + liftCalls.id,
                        Response.Listener<String> { response ->

                            // timer on ctreated state


                            var obj = JSONObject(response);

                            var buildObj = obj.getJSONObject("collection")
                                    .getJSONArray("items").getJSONObject(0).getJSONArray("data");
                            val lifCall = LiftCalls();

                            for (i in 0 until buildObj.length()) {
                                val item = buildObj.getJSONObject(i)

                                Log.d("A", "Response is: " +
                                        item)

                                if (item.get("name").equals("id")) {
                                    val buildingVal = item.getString("value").split(":");
                                    lifCall.id = buildingVal[2]
                                    lifCall.buildId = buildingVal[1]
                                } else if (item.get("name").equals("callState")) {

                                    if (!item.getString("value").equals("created")) {
                                        counter = 0;
                                    } else if (counter == 0L && item.getString("value").equals("created")) {
                                        counter = System.currentTimeMillis();
                                    }


                                    if (item.getString("value").equals("created") && counter > 0 && ((System.currentTimeMillis() - counter) >= 15000)) {
                                        lifCall.state = "cancelled";
                                        lifCall.cancel_reason = "Connection_TimeOut";
                                        System.out.println("counter state " + lifCall.state + " " + (System.currentTimeMillis() - counter))
                                        counter = 0;
                                    } else {
                                        lifCall.state = item.getString("value");
                                    }


                                } else if (item.get("name").equals("passengerGuidance")) {
                                    lifCall.passGuidance = item.getString("value");
                                } else if (item.get("name").equals("cancelReason")) {
                                    lifCall.cancel_reason = item.getString("value");
                                }




                                lifCall.source = liftCalls.source
                                lifCall.destination = liftCalls.destination

                            }

                            var linkObj = obj.getJSONObject("collection")
                                    .getJSONArray("items").getJSONObject(0).getJSONArray("links");

                            for (i in 0 until linkObj.length()) {
                                val item = linkObj.getJSONObject(i)

                                Log.d("A", "Response is: " +
                                        item)

                                if (item.get("rel").equals("car")) {
                                    val href = item.getString("href")
                                    lifCall.carLink = href
                                }

                                if (item.get("rel").equals("lift")) {
                                    val href = item.getString("href")
                                    lifCall.nameLink = href
                                }


                            }
                            System.out.println("counter state inside " + lifCall.state)
                            val acycTask = StoreDataAsync(this, LiftCalls::class.simpleName!!, "update", null, null, null, lifCall);
                            acycTask.setListner(this)
                            acycTask.execute();


                        },
                        Response.ErrorListener { volleyError ->

                            System.out.println("counter state" + volleyError)
                            counter = 0;
                            var message: String? = null
                            if (volleyError is NetworkError) {
                                message = "Cannot connect to Internet...Please check your connection!"
                            } else if (volleyError is ServerError) {
                                message = "The server could not be found. Please try again after some time!!"
                            } else if (volleyError is AuthFailureError) {
                                message = "Cannot connect to Internet...Please check your connection!"
                            } else if (volleyError is ParseError) {
                                message = "Parsing error! Please try again after some time!!"
                            } else if (volleyError is NoConnectionError) {
                                message = "Cannot connect to Internet...Please check your connection!"
                            } else if (volleyError is TimeoutError) {
                                message = "Connection TimeOut! Please check your internet connection."


                            }

                            Toast.makeText(this, message, Toast.LENGTH_LONG).show()
                        }

                ) {
                    override fun getHeaders(): MutableMap<String, String> {
                        val headers = HashMap<String, String>()
                        headers["accept"] = "application/vnd.collection+json"
                        headers["x-ibm-client-id"] = "319a265b-7568-4bdd-885d-c42e6c2044dc"
                        headers["x-ibm-client-secret"] = "H0yI6rO5qM5tQ6lV6jY3qF1kG2mT7dX2nC8lU3cR6dF3tP1yJ8"
                        return headers
                    }
                }
                stringRequest!!.setRetryPolicy(DefaultRetryPolicy(15000,
                        1,
                        DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
                queue.add(stringRequest)
            } else {
                counter = 0;
                if (liftCalls.state.equals("served")) {

                    val acycTask = StoreDataAsync(this, LiftCalls::class.simpleName!!, "delete", null, null, null, null);
                    acycTask.setListner(this)
                    acycTask.execute();
                }
            }
        }
    }

    override fun onUpdateLiftCalls() {

        intentReceiver = Intent("UPDATE_RESULT");

        sendBroadcast(intentReceiver)
    }


}