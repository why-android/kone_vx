package `in`.versionx.kone.Model

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey


@Entity
class LiftCalls {

    @PrimaryKey
    var id: String = ""

    @ColumnInfo(name = "state")
    var state: String = ""

    @ColumnInfo(name = "passGuidance")
    var passGuidance: String = ""

    @ColumnInfo(name = "buildId")
    var buildId: String = ""

    @ColumnInfo(name = "source")
    var source: String = ""

    @ColumnInfo(name = "destination")
    var destination: String = ""

    @ColumnInfo(name = "nmLink")
    var nameLink: String = ""


    @ColumnInfo(name = "carLink")
    var carLink: String = ""

    @ColumnInfo(name = "cancelReason")
    var cancel_reason:String=""


}