package `in`.versionx.kone.Database

import `in`.versionx.kone.Model.Area
import `in`.versionx.kone.Model.Lift
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.Query

@Dao
interface LiftDao {

    @Insert
    fun insertAll(vararg lift: Lift?)

    @Query("DELETE FROM Lift")
    fun deleteAll()
}
