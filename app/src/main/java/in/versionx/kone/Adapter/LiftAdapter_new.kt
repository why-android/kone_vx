package `in`.versionx.kone.Adapter

import `in`.versionx.kone.Model.Area
import `in`.versionx.kone.Model.Lift
import `in`.versionx.kone.Model.LiftCalls
import `in`.versionx.kone.R
import android.content.Context
import android.content.res.ColorStateList
import android.graphics.Color
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.recyclerview.widget.RecyclerView
import com.android.volley.NetworkResponse
import com.android.volley.Response
import com.android.volley.toolbox.JsonObjectRequest
import com.android.volley.toolbox.Volley
import kotlinx.android.synthetic.main.lift_box.view.*
import kotlinx.android.synthetic.main.lift_item.view.*
import org.json.JSONArray
import org.json.JSONObject


class LiftAdapter_new(val listItem: ArrayList<Area>, val context: Context) : RecyclerView.Adapter<LiftAdapter_new.ViewHolder>() {

    var listner: floorSelect? = null;
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val context = parent.context
        val inflater = LayoutInflater.from(context)

        // Inflate the custom layout

        // Inflate the custom layout
        val view: View = inflater.inflate(R.layout.lift_box, parent, false)

        // Return a new holder instance
        val viewHolder = ViewHolder(view)
        // Return a new holder instance

        return viewHolder
    }

    public fun setFloorListner(listner: floorSelect) {
        this.listner = listner;
    }

    override fun getItemViewType(position: Int): Int {
        return position
    }

    override fun getItemCount(): Int {

        return listItem.size
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {

        System.out.println("before clicked ")


        holder.floor.setText(listItem.get(position).nm)



        if (!listItem.get(position).iscurrent) {
            if (listItem.get(position).state == 0) {
                //  holder.cardView.cardView.setCardBackgroundColor(Color.parseColor("#CCB4CDCD"))

                holder.cardView.cardView.setCardBackgroundColor(Color.parseColor("#ffffff"))
            } else if (listItem.get(position).state == 1) {
                holder.cardView.cardView.setCardBackgroundColor(Color.parseColor("#ffff8800"))
            } else if (listItem.get(position).state == 2) {
                holder.cardView.cardView.setCardBackgroundColor(Color.parseColor("#ffff8800"))
            }

        } else {
            holder.cardView.cardView.setCardBackgroundColor(Color.parseColor("#E8E8E8"))

        }




            holder.itemView.setOnClickListener(View.OnClickListener {

                System.out.println("clicked "+!listItem.get(position).isSelected)


                if(!listItem.get(position).iscurrent) {

                    for (item in listItem) {
                        if (item.isSelected) {
                            Toast.makeText(context, "Please Scan the QR Code Again!", Toast.LENGTH_SHORT).show()
                            return@OnClickListener
                        }
                        /* item.isSelected = false;
                 holder.cardView.cardView.setCardBackgroundColor(Color.parseColor("#F5F5F5"))*/
                    }


                    if (!listItem.get(position).isSelected) {

                        if (listItem.get(position).state == 0 && !listItem.get(position).isSelected) {
                            holder.cardView.cardView.setCardBackgroundColor(Color.parseColor("#ffff8800"))
                            listItem.get(position).isSelected = true
                            listner!!.onFloorSelect();
                        } else if (listItem.get(position).state == 0 && listItem.get(position).isSelected) {
                            holder.cardView.cardView.setCardBackgroundColor(Color.parseColor("#ffff8800"))
                            listItem.get(position).isSelected = false
                            // listner!!.onFloorSelect();
                        }
                    }

                }
                else
                {
                    Toast.makeText(context, "Source And Destination Can't Be Same", Toast.LENGTH_SHORT).show()


                }
            })



    }

    class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {

        val cardView = view.cardView
        val floor = view.floor
    }

    interface floorSelect {
        public fun onFloorSelect();
    }


}