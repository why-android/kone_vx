package `in`.versionx.kone.Interface

import `in`.versionx.kone.Model.Area
import `in`.versionx.kone.Model.Building
import `in`.versionx.kone.Model.Lift
import `in`.versionx.kone.Model.LiftCalls

interface TaskFinishListner {

    fun OnAreaTaskFinishListner(area: List<Area>)
    fun OnBuildingTaskFinishListner(calls:List<Building>)
    fun OnLiftCallFinishListner(calls:List<LiftCalls>)
    fun onUpdateLiftCalls()
}